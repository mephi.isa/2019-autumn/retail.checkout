package com.example.checkout

import com.example.checkout.app.network.RestAPIOld
import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDateTime

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class Extension1EntryPoint {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    var API = RestAPIOld("localhost", 8080)
    @Test
    fun findTest(){
        var p = API.findPurchase(5)
        print(p)
    }
    @Test
    fun findRet(){
        var p = API.findReturn(1)
    }
    @Test
    fun updatePurch(){
        var pRowSet:MutableSet<PurchaseRow> = mutableSetOf()
        pRowSet.add(PurchaseRow(1000,181))
        pRowSet.add(PurchaseRow(2000,137))
        pRowSet.add(PurchaseRow(3000,144))
        pRowSet.add(PurchaseRow(4000,757))

        var p = PurchaseDTO(
            id = -1,
            cashier = 2,
            type = PurchaseType.PURCHASE,
            purchaseId = -1,
            status = Status.PAID,
            purchaseRowSet = pRowSet,
            create_dttm = LocalDateTime.now().toString(),
            discount_card = 0
        )
        var r = API.updatePurchase(p)
        print("\n")
        print(r)
        //print(BsonDocument.parse(p.json).getInt32("id").value)

    }
    @Test
    fun findTest1(){
        var Mg = MongoDBase("127.0.0.1",27017, "lab_admin", "610", "checkout")
        //var Mg = MongoDBase("bjczqvfva3wh1g4-mongodb.services.clever-cloud.com",27017, "uszgqwmucmlbc0223x8y", "Cq4ixfVmuNBU8MSVRIe9", "bjczqvfva3wh1g4")
        //var Mg = MongoClients.create("mongodb://uszgqwmucmlbc0223x8y:Cq4ixfVmuNBU8MSVRIe9@bjczqvfva3wh1g4-mongodb.services.clever-cloud.com:27017/bjczqvfva3wh1g4")
        Mg.findPurchase(5)
    }
    /*@Test
    fun updateTest(){
        var pRowSet:MutableSet<PurchaseRow> = mutableSetOf()
        pRowSet.add(PurchaseRow(1000,18))
        pRowSet.add(PurchaseRow(2000,17))
        pRowSet.add(PurchaseRow(3000,14))
        pRowSet.add(PurchaseRow(4000,77))

        var p = PurchaseDTO(
            id=5,
            cashier = 1,
            type = PurchaseType.PURCHASE,
            purchaseId = -1,
            status = Status.PAID,
            purchaseRowSet = pRowSet,
            create_dttm = LocalDateTime.now().toString(),
            discount_card = 0
            )
        var Mg = MongoDBase("bjczqvfva3wh1g4-mongodb.services.clever-cloud.com",27017, "uszgqwmucmlbc0223x8y", "Cq4ixfVmuNBU8MSVRIe9", "bjczqvfva3wh1g4")
        Mg.updatePurchase(p)

    }*/
}
