package com.example.checkout

import org.junit.Test

import org.junit.Assert.*


class CheckoutTest{
    var list: MutableList<Purchase> = arrayListOf()
    var scanner = ScannerMock()
    var product = ProductMock()
    var pricing = PricingMock()
    var printer = PrinterMock()
    var pinpad = PinPadMock()
    var connection = MongoDBase("127.0.0.1",27017, "lab_admin", "610", "checkout")
    var main_checkout = Checkout(scanner, pricing, printer, product, 1337, pinpad, connection)

    /*
    * Тест, проверяющий создание новой покупки
    * */
    @Test
    fun newPurchaseTest(){
        var obj = main_checkout.newPurchase(1)
        assertEquals(1, obj?.id)
        assertEquals(1337, obj?.cashier)
        assertEquals(Status.CREATED,obj?.status)
    }

    /*
    * Тесты, проверяющие создание нового возврата
    * */
    @Test
    fun newReturnTest(){
        main_checkout.newPurchase(2222)
        var obj = main_checkout.newReturn(1,2222)
        assertNotEquals(null, obj)
        assertEquals(1, obj?.id)
        assertEquals(2222, obj?.purchaseId)
        assertEquals(Status.CREATED,obj?.status)
    }
    @Test
    fun newReturnTestNeg_PurchaseIsNull(){
        var obj = main_checkout.newReturn(1,98)
        assertEquals(null, obj)
    }

    /*
        Тесты, проверяющие оплату и закрытие покупки
    */
    @Test
    fun payPurchaseTest(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1,1)
        var result = main_checkout.payPurchase(1)
        var cobj = connection.findPurchase(1)
        assertEquals(Status.PAID, cobj?.status)
        assertEquals(0, result)
    }
    @Test
    fun payPurchaseTestNeg_PruchaseIsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1,1)
        main_checkout.payPurchase(1)

        var result = main_checkout.payPurchase(1)
        var cobj = connection.findPurchase(1)
        assertEquals(Status.PAID, cobj?.status)
        assertEquals(-1, result)
    }
    @Test
    fun payPurchaseTestNeg_PurchaseIsNull(){
        var result = main_checkout.payPurchase(2)
        assertEquals(-1, result)
    }
    /*
        Тесты, проверяющие отказ от покупки
    */
    @Test
    fun declinePurchaseTest(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1,1)
        var result = main_checkout.declinePurchase(1)
        var cobj = connection.findPurchase(1)
        assertEquals(Status.DECLINED, cobj?.status)
        assertEquals(0, result)
    }
    @Test
    fun declinePurchaseTestNeg_PurchaseIsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1,1)
        main_checkout.payPurchase(1)

        var result = main_checkout.declinePurchase(1)
        var cobj = connection.findPurchase(1)
        assertEquals(Status.PAID, cobj?.status)
        assertEquals(-1, result)
    }
    @Test
    fun declinePurchaseTestNeg_PurchaseIsNull(){
        var result = main_checkout.declinePurchase(2)
        assertEquals(-1, result)
    }
    /*
        Тесты, проверяющие поиск продукта в покупке
    */
    @Test
    fun gettProductTest(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,10,24)
        var pRow = main_checkout.getProduct(1, 10)
        assertEquals(10,pRow?.product_code)
        assertEquals(24,pRow?.count)
    }
    @Test
    fun getProductTestNeg_PurchIsNull(){
        var pRow = main_checkout.getProduct(11, 10)
        assertNull(pRow)
    }
    @Test
    fun getProductTestNeg_ProductIsNull(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,10,24)
        var pRow = main_checkout.getProduct(1, 12)
        assertNull(pRow)
    }

    /*
        Тесты, проверяющие добавление продукта в покупку
    */
    @Test
    fun addProductToPurchaseTest(){
        var obj = main_checkout.newPurchase(1)
        var result = main_checkout.addProductToPurchase(1,1822,7)
        var cobj = connection.findPurchase(1)
        assertEquals(1, cobj?.purchaseRowSet?.count { it.product_code == 1822 })
        assertEquals(0, result)
    }
    @Test
    fun addProductToPurchaseTestNeg_PurchaseIsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.payPurchase(1)
        var result = main_checkout.addProductToPurchase(1,1822,7)
        var cobj = connection.findPurchase(1)
        assertEquals(-1, result)
        assertEquals(0, cobj?.purchaseRowSet?.count { it.product_code == 1822 })
    }
    @Test
    fun addProductToPurchaseTestNeg_PurchaseIsNull(){
        var result = main_checkout.addProductToPurchase(12,1822,7)
        assertEquals(-1, result)
    }

    /*
        Тесты, проверяющие удаление продукта из покупки
    */
    @Test
    fun removeProductTest(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1000, 1)
        main_checkout.addProductToPurchase(1, 2000, 2)
        var result = main_checkout.removeProductFromPurchase(1,1000)
        var cobj = connection.findPurchase(1)
        assertEquals(1,cobj!!.purchaseRowSet.count())
        assertEquals(0, result)
    }
    @Test
    fun removeProductTestNeg_Purchase_IsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1,1000, 1)
        main_checkout.addProductToPurchase(1, 2000, 2)
        main_checkout.payPurchase(1)
        var result = main_checkout.removeProductFromPurchase(1,1000)
        var cobj = connection.findPurchase(1)
        assertEquals(2,cobj!!.purchaseRowSet.count())
        assertEquals(-1, result)
    }
    @Test
    fun removeProductTestNeg_Purchase_IsNull(){
        var result = main_checkout.removeProductFromPurchase(31415926,1000)
        assertEquals(-1, result)
    }

    /*
        Тесты, проверяющие добавление скидочной карты
    */
    @Test
    fun adddDiscountCardTest(){
        var obj = main_checkout.newPurchase(1)
        var result = main_checkout.addDiscountCard(1, 133)
        var cobj = connection.findPurchase(1)
        assertEquals(133, cobj?.discount_card)
        assertEquals(0, result)
    }
    @Test
    fun addDiscountCardTestNeg_PurchaseIsNull(){
        var result = main_checkout.addDiscountCard(1, 133)
        assertEquals(-1, result)
    }
    @Test
    fun addDiscountCardTestNeg_PurchaseIsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.payPurchase(1)
        var result = main_checkout.addDiscountCard(1, 133)
        var cobj = connection.findPurchase(1)
        assertEquals(0, cobj?.discount_card)
        assertEquals(-1, result)
    }
    @Test
    fun addDiscountCardTestNeg_CardIsNot0(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addDiscountCard(1, 133)
        var result = main_checkout.addDiscountCard(1, 132)
        var cobj = connection.findPurchase(1)
        assertEquals(133, cobj?.discount_card)
        assertEquals(-1, result)
    }

    /*
        Тесты, проверяющие удаление скидочной карты
    */
    @Test
    fun removeDiscountCardTest(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addDiscountCard(1, 133)
        var result = main_checkout.removeDiscountCard(1)
        var cobj = connection.findPurchase(1)
        assertEquals(0, cobj?.discount_card)
        assertEquals(0, result)
    }
    @Test
    fun removeDiscountCardTestNeg_PurchaseIsNull(){
        var result = main_checkout.removeDiscountCard(1)
        assertEquals(-1, result)
    }
    @Test
    fun removeDiscountCardTestNeg_PurchaseIsClosed(){
        var obj = main_checkout.newPurchase(1)
        main_checkout.addDiscountCard(1, 133)
        main_checkout.payPurchase(1)
        var result = main_checkout.removeDiscountCard(1)
        var cobj = connection.findPurchase(1)
        assertEquals(133, cobj?.discount_card)
        assertEquals(-1, result)
    }
    @Test
    fun removeDiscountCardTestNeg_CardIs0(){
        var obj = main_checkout.newPurchase(1)
        var result = main_checkout.removeDiscountCard(1)
        var cobj = connection.findPurchase(1)
        assertEquals(0, cobj?.discount_card)
        assertEquals(-1, result)
    }

    @Test
    fun getPurchaseInfoTest(){
        main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1, 1000 , 1)
        main_checkout.addProductToPurchase(1, 2000 , 2)
        main_checkout.addProductToPurchase(1, 3000 , 3)
        main_checkout.addProductToPurchase(1, 4000 , 4)
        main_checkout.addProductToPurchase(1, 5000 , 5)
        main_checkout.addDiscountCard(1,20)
        var pInfo = main_checkout.getPurchaseInfo(1)
        assertEquals(20, pInfo.discount_card_id)
        assertEquals(1155,pInfo.sum_price)
        assertEquals(5,pInfo.sum_discount)
        assertEquals(1150, pInfo.price)
        assertEquals(5, pInfo.purchaseRows.count())
        assertEquals("ABC", pInfo.purchaseRows.find { it.product_code == 1000 }?.name)
        assertEquals(1, pInfo.purchaseRows.find { it.product_code == 1000 }?.discount)
        assertEquals(118, pInfo.purchaseRows.find { it.product_code == 1000 }?.price)
        assertEquals(117, pInfo.purchaseRows.find { it.product_code == 1000 }?.result_price)
        assertEquals(1, pInfo.purchaseRows.count{it.product_code == 1000})
        assertEquals(1, pInfo.purchaseRows.count{it.product_code == 2000})
        assertEquals(1, pInfo.purchaseRows.count{it.product_code == 3000})
        assertEquals(1, pInfo.purchaseRows.count{it.product_code == 4000})
        assertEquals(1, pInfo.purchaseRows.count{it.product_code == 5000})
        assertEquals(5, pInfo.purchaseRows.count{it.discount > 0})
        assertEquals(5, pInfo.purchaseRows.count{it.price > 0})

    }

    @Test
    fun getPurchaseInfoTestNeg_PurchaseIsNull(){
        var pInfo = main_checkout.getPurchaseInfo(1)
        assertEquals(0, pInfo.purchaseRows.count())
    }

    /*
        Тест, проверяющий получение общей стоимости покупок
    */
    @Test
    fun getPurchasePriceTest(){
        main_checkout.newPurchase(1)
        main_checkout.addProductToPurchase(1, 1000 , 1) // Price = 118
        main_checkout.addProductToPurchase(1, 2000 , 2) // Price = 102*2 = 204
        assertEquals(321, main_checkout.getPurchasePrice(1))
    }

    @Test
    fun getPurchasePriceTestNeg_PurchaseIsNull(){
        assertEquals(0, main_checkout.getPurchasePrice(1))
    }

    /*
        Тесты, проверяющие получение счёта за покупку с учетом возвратов
    */
    @Test
    fun billTest_withReturns(){
        main_checkout.newPurchase(1)
        var purch = connection.findPurchase(1)
        main_checkout.addProductToPurchase(purch!!.id, 1000,1)
        main_checkout.addProductToPurchase(purch.id, 2000,16)
        main_checkout.addProductToPurchase(purch.id, 3000,1)
        main_checkout.addProductToPurchase(purch.id, 4000,1)
        main_checkout.addProductToPurchase(purch.id, 5000,6)
        main_checkout.addDiscountCard(purch.id,1111)
        main_checkout.payPurchase(1)

        main_checkout.newReturn(2,1)
        main_checkout.addProductToPurchase(2,2000,1)
        main_checkout.payPurchase(2)
        main_checkout.newReturn(3,1)
        main_checkout.addProductToPurchase(3,2000,5)
        main_checkout.addProductToPurchase(3,5000,1)
        main_checkout.payPurchase(3)

        var bill = main_checkout.getBill(1)
        assertEquals(5, bill.purchaseRows.count())
        assertEquals(1, bill.purchaseRows.find { it.product_code == 1000 }?.count)
        assertEquals(10, bill.purchaseRows.find { it.product_code == 2000 }?.count)
        assertEquals(1, bill.purchaseRows.find { it.product_code == 3000 }?.count)
        assertEquals(1, bill.purchaseRows.find { it.product_code == 4000 }?.count)
        assertEquals(5, bill.purchaseRows.find { it.product_code == 5000 }?.count)
    }
    @Test
    fun billTestNeg_PurchaseIsNull(){
        var bill = main_checkout.getBill(1)
        assertEquals(bill.purchaseRows.count(), 0)
    }
    @Test
    fun billTest_NoReturns(){
        main_checkout.newPurchase(122)
        var purch = connection.findPurchase(122)
        main_checkout.addProductToPurchase(purch!!.id, 1000,1)
        main_checkout.addProductToPurchase(purch.id, 2000,16)
        main_checkout.addProductToPurchase(purch.id, 3000,1)
        main_checkout.addProductToPurchase(purch.id, 4000,1)
        main_checkout.addProductToPurchase(purch.id, 5000,6)
        main_checkout.addDiscountCard(purch.id,1111)
        main_checkout.payPurchase(1)

        var bill = main_checkout.getBill(122)
        assertEquals(5, bill.purchaseRows.count())
        assertEquals(1, bill.purchaseRows.find { it.product_code == 1000 }?.count)
        assertEquals(16, bill.purchaseRows.find { it.product_code == 2000 }?.count)
        assertEquals(1, bill.purchaseRows.find { it.product_code == 3000 }?.count)
        assertEquals(1, bill.purchaseRows.find { it.product_code == 4000 }?.count)
        assertEquals(6, bill.purchaseRows.find { it.product_code == 5000 }?.count)
    }

}