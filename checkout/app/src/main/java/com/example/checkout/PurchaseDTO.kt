package com.example.checkout

import java.time.LocalDateTime

data class PurchaseDTO(
    val id:Int,
    val cashier:Int,
    val type:PurchaseType,
    val purchaseId:Int,
    val status:Status,
    val purchaseRowSet: MutableSet<PurchaseRow> = mutableSetOf<PurchaseRow>(),
    val create_dttm: String,
    val discount_card:Int
) {
}