package com.example.checkout

class PurchaseInfo(
   var purchaseRows:MutableList<PurchaseRowDTOview>,
   var discount_card_id:Int,
   var price:Int,
   var sum_discount:Int,
   var sum_price:Int
) {
}