//package com.example.checkout.app.network
//
//
//import com.example.checkout.DBase
//import com.example.checkout.Purchase
//import com.example.checkout.PurchaseDTO
//import com.google.gson.Gson
//import okhttp3.MediaType
//import okhttp3.OkHttpClient
//import okhttp3.Request
//import okhttp3.RequestBody
//import org.litote.kmongo.json
//import java.time.LocalDateTime
//
//@Deprecated("Use new version")
//class RestAPIOld(
//    val server: String,
//    val port: Int
//) : DBase {
//
//    private var url = "http://" + server + ":" + port.toString()
//    private var client = OkHttpClient()
//    override fun updatePurchase(purchaseDTO: PurchaseDTO): Int {
//        var rBody =
//            RequestBody.create(MediaType.parse("application/json; charset=utf-8"), purchaseDTO.json)
//        var req = Request.Builder()
//            .url(url + "/update")
//            .post(rBody)
//            .build()
//        var response = client.newCall(req).execute()
//        var jsonObject = response.body()!!.string()
//        var g = Gson()
//        var r = g.fromJson<UpdResult>(jsonObject, UpdResult::class.java)
//        return r.id
//    }
//
//    override fun findPurchase(id: Int): Purchase? {
//        var jsonObject = get(url + "/get_purchase?id=" + id.toString())
//        val gs = Gson()
//        var pDTO = gs.fromJson<PurchaseDTO>(jsonObject, PurchaseDTO::class.java)
//        var purchase = convertDTOtoPurchase(pDTO)
//        return purchase
//    }
//
//    override fun findReturn(purchase_id: Int): List<Purchase> {
//        var jsonObject = get(url + "/get_return?purchase_id=" + purchase_id.toString())
//        val gs = Gson()
//        var pDTO = gs.fromJson(jsonObject, Array<PurchaseDTO>::class.java)
//        var purchases = mutableListOf<Purchase>()
//        for (i in pDTO) {
//            purchases.add(convertDTOtoPurchase(i))
//        }
//        //var purchase = convertDTOtoPurchase(pDTO)
//        return purchases
//    }
//
//    private fun convertDTOtoPurchase(pDTO: PurchaseDTO): Purchase {
//        var purchase = Purchase(
//            id = pDTO.id,
//            cashier = pDTO.cashier,
//            type = pDTO.type,
//            purchaseId = pDTO.purchaseId,
//            status = pDTO.status,
//            purchaseRowSet = pDTO.purchaseRowSet,
//            create_dttm = LocalDateTime.parse(pDTO.create_dttm)
//        )
//        purchase.discount_card = pDTO.discount_card
//        return purchase
//    }
//
//    private fun get(url: String): String {
//        var req = Request.Builder()
//            .url(url)
//            .build()
//        var response = client.newCall(req).execute()
//        var ans = response.body()?.string()
//        if (ans == null)
//            return "{\"error\": \"NotFound\"}"
//        else
//            return ans
//    }
//}