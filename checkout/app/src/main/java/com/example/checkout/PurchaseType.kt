package com.example.checkout

enum class PurchaseType {
    PURCHASE,
    RETURN
}