package com.example.checkout.app.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*

fun <T> MutableLiveData<T>.onNext(next: T) {
    this.value = next
}

inline fun <reified T : ViewModel> FragmentActivity.obtainViewModel(
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

inline fun <reified T : ViewModel> FragmentActivity.obtainViewModel(
    viewModelFactory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val vm = ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    vm.body()
    return vm
}

inline fun <reified T : ViewModel> Fragment.obtainViewModel(
    viewModelFactory: ViewModelProvider.Factory? = null
): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

inline fun <reified T : ViewModel> Fragment.obtainViewModel(
    viewModelFactory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val vm = ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    vm.body()
    return vm
}

inline fun <reified T : Any, reified L : LiveData<T>> LifecycleOwner.observe(
    liveData: L,
    noinline block: (T) -> Unit
) {
    liveData.observe(this, Observer<T> { it?.let { block.invoke(it) } })
}
//
//inline fun <reified T : Any, reified L : CommandsLiveData<T>> LifecycleOwner.observe(
//    liveData: L,
//    noinline block: (T) -> Unit
//) {
//    liveData.observe(this, Observer<LinkedList<T>> { commands ->
//        if (commands == null) {
//            return@Observer
//        }
//        var command: T?
//        do {
//            command = commands.poll()
//            if (command != null) {
//                block.invoke(command)
//            }
//        } while (command != null)
//    })
//}
