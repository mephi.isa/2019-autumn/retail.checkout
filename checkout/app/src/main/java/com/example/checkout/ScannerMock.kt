package com.example.checkout

import kotlin.random.Random

class ScannerMock : Scanner {
    override fun scan():Int = Random.nextInt(0,2555)
}