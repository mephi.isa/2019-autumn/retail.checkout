package com.example.checkout

data class PurchaseRowDTO(
    var product_code:Int,
    var count: Int,
    var type:ProductType,
    var discount:Int
) {
}