package com.example.checkout

import java.time.LocalDateTime

data class Purchase(
    var id:Int,
    var cashier:Int,
    var type:PurchaseType = PurchaseType.PURCHASE,
    var purchaseId:Int = -1,
    var status:Status = Status.CREATED,
    var purchaseRowSet: MutableSet<PurchaseRow> = mutableSetOf<PurchaseRow>(),
    var create_dttm: LocalDateTime = LocalDateTime.now()
) {
    var discount_card:Int = 0
    get() = field
    set(value){
        if(value >= 0){
            field = value
        }
    }



    fun decline(){
        this.status = Status.DECLINED
    }

    fun close(){
        this.status = Status.PAID
    }

    fun removeProduct(product_code:Int){
        if(this.status == Status.CREATED) {
            print("\n\nCNT BEFORE = "+ purchaseRowSet.count())
            var d = purchaseRowSet.find{it.product_code == product_code}
            print("\n\nP =  " + d.toString())
            var result = purchaseRowSet.removeIf { it.product_code == product_code }
            print("\n\nCNT AFTER = " + purchaseRowSet.count() + "; result = "+result.toString())
        }
    }

    fun findProduct(product_code:Int): PurchaseRow? = purchaseRowSet.find { it.product_code == product_code }

    fun addProduct(row:PurchaseRow){
        if(this.status == Status.CREATED) {
            var prod = findProduct(product_code = row.product_code)
            if(prod == null){
                purchaseRowSet.add(row)
            }
            else{
                prod.count += row.count
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Purchase

        if (id != other.id) return false
        if (cashier != other.cashier) return false
        if (type != other.type) return false
        if (purchaseId != other.purchaseId) return false
//        if (status != other.status) return false
//        if (purchaseRowSet != other.purchaseRowSet) return false
        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + cashier
        result = 31 * result + type.hashCode()
        result = 31 * result + purchaseId
//        result = 31 * result + status.hashCode()
//        result = 31 * result + purchaseRowSet.hashCode()
        return result
    }


}