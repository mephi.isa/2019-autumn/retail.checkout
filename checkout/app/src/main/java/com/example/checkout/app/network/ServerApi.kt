package com.example.checkout.app.network

import com.example.checkout.Purchase
import com.example.checkout.PurchaseDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query


interface ServerApi {

    @GET("update")
    fun updatePurchase(@Body purchaseDTO: PurchaseDTO): Call<UpdResult>

    @GET("get_purchase")
    fun findPurchase(@Query("id") id: Int): Call<Purchase>

    @GET("get_return")
    fun findReturn(@Query("purchaseId") id: Int): Call<List<Purchase>>

}
