package com.example.checkout

interface Scanner {
    fun scan(): Int
}