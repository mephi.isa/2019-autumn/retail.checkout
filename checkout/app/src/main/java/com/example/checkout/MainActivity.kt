package com.example.checkout

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

//import sun.jvm.hotspot.utilities.IntArray


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        initView()
    }

    fun updateInfo(checkout: Checkout, view: TableLayout, purchase: PurchaseInfo) {
        view.removeAllViews()
        var txtView = TextView(this)
        txtView.text = "Code\t\tname\t\t\tcount\t\tprice\t\tdiscount\t\tresult_price"
        view.addView(txtView)
        for (i in purchase.purchaseRows) {
            var txtView = TextView(this)
            var txt = listOf(
                i.product_code,
                i.name,
                i.count,
                i.price,
                i.discount,
                i.result_price
            ).joinToString("\t\t")
            txtView.text = txt
            view.addView(txtView)
            view.addView(Button(this).apply {
                text = "Изменить количество"
                setOnClickListener {
                    /*var dlg = AlertDialog.Builder(this)
                    //val promptsView = .inflate(R.layout.alert_dialog, null)
                    val view = layoutInflater.inflate(R.layout.alert_dialog, null)
                    val edit = view.findViewById(R.id.etUserInput) as EditText
                    dlg.setTitle("Изменение количества")
                    dlg.setMessage("Введите количество")
                    dlg.setView(view)
                    dlg.setPositiveButton(android.R.string.yes) { dialog, which ->

                    }
                    dlg.setNegativeButton(android.R.string.no) { dialog, which ->
                        showMsg("Действие отменено")
                    }
                    dlg.show()*/
                }
            })
            view.addView(Button(this).apply {
                text = "Удалить"
                setOnClickListener {

                }
            })
        }

    }

    fun showMsg(text: String) {
        Toast.makeText(this@MainActivity, text, Toast.LENGTH_SHORT).show()
    }

//    private fun initView() {
//        var scanner = ScannerMock()
//        var pricing = PricingMock()
//        var product = ProductMock()
//        var printer = PrinterMock()
//        var cashier = 1
//        var pinPad = PinPadMock()
//        //var DBConnect = MongoDBase("127.0.0.1",27017, "lab_admin", "610", "checkout")
//        var DBConnect = DBaseMock()
//        var checkout = Checkout(
//            scanner = scanner,
//            pricing = pricing,
//            printer = printer,
//            product = product,
//            cashier = cashier,
//            pinPad = pinPad,
//            DBConnect = DBConnect
//        )
//
//
//        val btnScan = findViewById<Button>(R.id.btnScan)
//        val btnPay = findViewById<Button>(R.id.btnPay)
//        val btnDecline = findViewById<Button>(R.id.btnDecline)
//        val btnNewPurch = findViewById<Button>(R.id.btnNewPurchase)
//        val btnNewRet = findViewById<Button>(R.id.btnNewReturn)
//        val purchaseView = findViewById<TableLayout>(R.id.table)
//        val price = findViewById<TextView>(R.id.tvPrice)
//        val discount = findViewById<TextView>(R.id.tvDiscount)
//        val result_price = findViewById<TextView>(R.id.tvSumPrice)
//        val tvCashier = findViewById<TextView>(R.id.tvCashier)
//        //purchaseView.layoutManager = LinearLayoutManager(this)
//        var current_purchase_id = -1
//
//        tvCashier.text = "ИД кассира: " + checkout.cashier.toString()
//        tvPrice.text = "Сумма покупок: 0"
//        tvDiscount.text = "Сумма скидки: 0"
//        tvSumPrice.text = "Сумма покупок с учетом скидки: 0"
//        btnScan.setOnClickListener {
//            var prod_id = scanner.scan()
//            var r = checkout.addProductToPurchase(current_purchase_id, prod_id, 1)
//            if (r == -1) {
//                showMsg("AddProduct: ERROR")
//            } else {
//                showMsg("AddProduct: " + prod_id)
//                var info = checkout.getPurchaseInfo(current_purchase_id)
//                updateInfo(checkout, purchaseView, info)
//                tvPrice.text = "Сумма покупок: " + info.price.toString()
//                tvDiscount.text = "Сумма скидки: " + info.sum_discount
//                tvSumPrice.text = "Сумма покупок с учетом скидки: " + info.sum_price.toString()
//            }
//
//        }
//
//        btnNewPurch.setOnClickListener {
//            var purchase = checkout.newPurchase(1)
//            if (purchase == null) {
//                showMsg("NULL")
//            } else {
//                current_purchase_id = purchase.id
//                //var txtView = TextView(this)
//                //txtView.setText("Покупка номер: " + purchase.id)
//                //purchaseView.addView(txtView)
//            }
//        }
//
//        btnPay.setOnClickListener {
//            var dlg = AlertDialog.Builder(this)
//            dlg.setTitle("Оплата покупки")
//            dlg.setMessage("Запросить оплату покупки?")
//            dlg.setPositiveButton(android.R.string.yes) { dialog, which ->
//                if (checkout.payPurchase(current_purchase_id) == 0) {
//                    showMsg("Покупка оплачена!")
//                    purchaseView.removeAllViews()
//                } else {
//                    showMsg("Возникла ошибка при оплате")
//                }
//            }
//            dlg.setNegativeButton(android.R.string.no) { dialog, which ->
//                showMsg("Отмена оплаты")
//            }
//            dlg.show()
//        }
//
//        btnDecline.setOnClickListener {
//            var dlg = AlertDialog.Builder(this)
//
//            //val view = layoutInflater.inflate(R.layout.alert_dialog, null)
//
//            //val categoryEditText = view.findViewById(R.id.categoryEditText) as EditText
//
//            //dlg.setView(view)
//            dlg.setTitle("Отмена покупки")
//            dlg.setMessage("Отменить покупку")
//            dlg.setPositiveButton(android.R.string.yes) { dialog, which ->
//                checkout.declinePurchase(current_purchase_id)
//                showMsg("Покупка отклонена!")
//                purchaseView.removeAllViews()
//            }
//            dlg.setNegativeButton(android.R.string.no) { dialog, which ->
//                showMsg("Действие отменено")
//            }
//            dlg.show()
//        }
//
//        btnNewRet.setOnClickListener {
//            var dlg = AlertDialog.Builder(this)
//            //val promptsView = .inflate(R.layout.alert_dialog, null)
//            val view = layoutInflater.inflate(R.layout.alert_dialog, null)
//            val edit = view.findViewById(R.id.etUserInput) as EditText
//            dlg.setTitle("Возврат")
//            dlg.setMessage("Введите номер чека")
//            dlg.setView(view)
//            dlg.setPositiveButton(android.R.string.yes) { dialog, which ->
//                val id: Int = edit.text.toString().toInt()
//                var purch = DBConnect.findPurchase(id)
//                if (purch == null) {
//                    showMsg("Покупка не найдена!")
//                } else {
//                    showMsg("Возврат создан!")
//                    current_purchase_id = purch.id
//                }
//            }
//            dlg.setNegativeButton(android.R.string.no) { dialog, which ->
//                showMsg("Действие отменено")
//            }
//            dlg.show()
//        }
//    }

}
