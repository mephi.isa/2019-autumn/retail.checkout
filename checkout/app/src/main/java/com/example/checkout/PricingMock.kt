package com.example.checkout

import kotlin.random.Random

class PricingMock : Pricing {
    override fun getPrice(barcode: Int):Int = (barcode * 127) % 133
    override fun getDiscount(card_id:Int, bill: MutableSet<PurchaseRow>):MutableSet<PurchaseRow>{
        var result = bill
        for(i in result){
            i.discount = 1
        }
        return result
    }
}