package com.example.checkout.app.presentation

import com.example.checkout.PurchaseInfo
import com.example.checkout.PurchaseRowDTOview
import com.example.checkout.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_purchase.*

data class PurchaseItem(
    private val purchaseInfo: PurchaseRowDTOview,
    private val onIncClick: (PurchaseRowDTOview) -> Unit,
    private val onDecClick: (PurchaseRowDTOview) -> Unit,
    private val onRemClick: (PurchaseRowDTOview) -> Unit
) : Item() {

    override fun getLayout(): Int = R.layout.item_purchase

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder) {
            prod_id.text = purchaseInfo.product_code.toString()
            prod_name.text = purchaseInfo.name
            prod_count.text = purchaseInfo.count.toString()
            prod_price.text = purchaseInfo.price.toString()
            prod_discount.text = purchaseInfo.discount.toString()
            prod_sum_price.text = purchaseInfo.result_price.toString()
            btnInc.setOnClickListener { onIncClick(purchaseInfo) }
            btnDec.setOnClickListener { onDecClick(purchaseInfo) }
            btnRemove.setOnClickListener { onRemClick(purchaseInfo) }

//            container.setOnClickListener { onClick(assignment) }
        }
    }
}
