package com.example.checkout
import com.google.gson.Gson
import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClients
import com.mongodb.connection.Server
import org.bson.BsonDocument
import org.bson.BsonElement
import org.bson.BsonString
import org.bson.Document
import org.litote.kmongo.json
import java.time.LocalDateTime

@Suppress("DEPRECATION")
class MongoDBase(
    host:String,
    port:Int,
    username:String,
    password:String,
    db_name:String
) : DBase{

    val credentials = MongoCredential.createCredential(username, db_name, password.toCharArray())
    //val connectionString = "mongodb://" + username + ":" + password + "@" + db_name + ":" + port.toString();
    var opts = MongoClientOptions.builder()
        .connectTimeout(0)
        .maxConnectionIdleTime(0)
        .maxWaitTime(0)
        .socketTimeout(3000000)
        .serverSelectionTimeout(500000)
        .build()
    var client = MongoClient(ServerAddress(host, port), arrayListOf(credentials), opts)
    //var client = MongoClients.create("mongodb://uszgqwmucmlbc0223x8y:Cq4ixfVmuNBU8MSVRIe9@bjczqvfva3wh1g4-mongodb.services.clever-cloud.com:27017/bjczqvfva3wh1g4")
    var database = client.getDatabase(db_name)
    private var collection = database.getCollection("purchases", BsonDocument::class.java)

    override fun updatePurchase(pr: PurchaseDTO): Int {
        var query = BsonDocument.parse(pr.json)
        //var filter = BsonDocument.parse("{\"id\": "+pr.id.toString()+"}")
        var filter = Document("id", pr.id)
        var found = collection.find(filter).count()
        if(found > 0) {
            collection.replaceOne(filter, query)
        }
        else{
            collection.insertOne(query)
        }
        return 1
    }

    override fun findPurchase(id:Int):Purchase?{
        var query = Document("id", id)
        var foundObjects = collection.find(query)
        var jsonObject:String = ""

        if(foundObjects.count() == 0){
            return null
        }

        for(i in foundObjects){
            jsonObject = i.toJson()
        }
        val gs = Gson()
        var pDTO = gs.fromJson<PurchaseDTO>(jsonObject, PurchaseDTO::class.java)
        var purchase = convertDTOtoPurchase(pDTO)
        return purchase
    }

    override fun findReturn(purchase_id: Int): List<Purchase> {
        var query = Document("purchaseId", purchase_id).append("type", "RETURN")
        var foundObjects = collection.find(query)
        var purchases: MutableList<Purchase> = mutableListOf()
        var pDTO:PurchaseDTO
        var gs = Gson()
        for(i in foundObjects){
            pDTO = gs.fromJson<PurchaseDTO>(i.toJson(), PurchaseDTO::class.java)
            purchases.add(convertDTOtoPurchase(pDTO))
        }
        return purchases.toList()
    }

    private fun convertDTOtoPurchase(pDTO:PurchaseDTO):Purchase{
        var purchase = Purchase(
            id = pDTO.id,
            cashier = pDTO.cashier,
            type = pDTO.type,
            purchaseId = pDTO.purchaseId,
            status = pDTO.status,
            purchaseRowSet = pDTO.purchaseRowSet,
            create_dttm = LocalDateTime.parse(pDTO.create_dttm)
        )
        purchase.discount_card = pDTO.discount_card
        return purchase
    }
}
