package com.example.checkout

class Checkout(
    var scanner: Scanner,
    var pricing: Pricing,
    var printer: Printer,
    var product: Product,
    var cashier: Int,
    var pinPad: PinPad,
    var DBConnect: DBase
) {

    /*
    *    Создание новой покупки.
    *    Возвращает DataTransferObject для Purchase
    */
    fun newPurchase(
        id: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var objPurchase = Purchase(id, this.cashier)
        DBConnect.updatePurchase(
            this.PurchaseToDTO(objPurchase),
            onSuccess = { purchase ->
                //onSuccessNewPurchase(objPurchase, onSuccess, onError)
                onSuccess.invoke()
            },

            onError = { throwable ->
                onError.invoke(throwable)
            }
        )
    }

    private fun onSuccessNewPurchase(
        purchase: Purchase,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {

    }

    /*
    *   Создание нового возврата.
    *   Возвращает DataTransferObject для Purchase
    */
    fun newReturn(
        id: Int,
        purchase_id: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var purchase = DBConnect.findPurchase(purchase_id,
            onSuccess = { purchase ->
                onSuccessNewReturn(purchase, onSuccess, onError)
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )
        TODO("сделать создание возврата")
    }

    fun onSuccessNewReturn(
        purchase: Purchase,
        onSuccess: (() -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        val purchaseDTO = PurchaseToDTO(purchase)

    }
//    fun newReturn1(id: Int, purchaseId: Int): PurchaseDTO? {
//        var purchase = DBConnect.findPurchase(purchaseId)
//        var rets = DBConnect.findReturn(purchaseId)
//        for (i in rets) {
//            if (i.status == Status.CREATED) {
//                return null
//            }
//        }
//        if (purchase != null) {
//            var objReturn = Purchase(
//                id = id,
//                purchaseId = purchaseId,
//                type = PurchaseType.RETURN,
//                cashier = this.cashier
//            )
//            DBConnect.updatePurchase(this.PurchaseToDTO(objReturn))
//            return PurchaseToDTO(objReturn)
//        } else {
//            return null
//        }
//    }

    /*
    *   Закрытие покупки с отказным результатом
    *   Кейс: покупатель отказался от покупок
    *   Возвращает 0 в случае успеха (покупка найдена и открыта)
    *   и -1 в случае отказа
    * */

    fun declinePurchase(
        purchase_id: Int,
        onSuccess: (() -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        DBConnect.findPurchase(
            purchaseId = purchase_id,
            onSuccess = { purchase ->
                onDeclinePurchaseSuccess(purchase_id, purchase, onSuccess, onError)
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )
    }

    private fun onDeclinePurchaseSuccess(
        purchase_id: Int, purchase: Purchase,
        onSuccess: (() -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        val price = getPurchasePrice(purchase_id)
        val payment = pinPad.card_pay(price)

        if (payment && purchase.status == Status.CREATED) {
            purchase.decline()
            DBConnect.updatePurchase(PurchaseToDTO(purchase),
                onSuccess = {
                    onSuccess.invoke()
                },
                onError = { throwable ->
                    onError.invoke(throwable)
                }
            )
        }
    }

    /*
    *   Оплата и закрытие покупки.
    *   Метод вызывает блокирующую функцию взаимодействия с пин-падом, после работы с которым
    *   покупка закрывается. Метод возвращает 0 в случае успеха (покупка найдена, открыта и
    *   оплата проведена) и -1 в случае ошибки
    * */
    fun payPurchase(
        purchase_id: Int,
        onSuccess: (() -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        DBConnect.findPurchase(
            purchaseId = purchase_id,
            onSuccess = { purchase ->
                onPayPurchaseSuccess(purchase_id, purchase, onSuccess, onError)
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )

        /*var price = getPurchasePrice(purchase_id)
        var payment = pinPad.card_pay(price)
        if (payment && purchase != null && purchase.status == Status.CREATED) {
            purchase.close()
            DBConnect.updatePurchase(this.PurchaseToDTO(purchase))
            printer.print_bill(getPurchaseInfo(purchase_id))
            return 0
        } else {
            return -1
        }*/
    }

    private fun onPayPurchaseSuccess(
        purchase_id: Int, purchase: Purchase,
        onSuccess: (() -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        val price = getPurchasePrice(purchase_id)
        val payment = pinPad.card_pay(price)

        if (payment && purchase.status == Status.CREATED) {
            purchase.close()
            DBConnect.updatePurchase(PurchaseToDTO(purchase),
                onSuccess = {
                    printer.print_bill(getPurchaseInfo(purchase_id))
                    onSuccess.invoke()
                },
                onError = { throwable ->
                    onError.invoke(throwable)
                }
            )
        }
    }


//    fun declinePurchase(purchase_id: Int): Int {
//        var purchase = DBConnect.findPurchase(purchase_id)
//        if (purchase != null && purchase.status == Status.CREATED) {
//            purchase.decline()
//            DBConnect.updatePurchase(this.PurchaseToDTO(purchase))
//            return 0
//        } else {
//            return -1
//        }
//    }

    /*
    *   Поиск продукта в покупке
    *   Возвращает DTO продукта или null в случае,
    *   если продукт с кодом product_code не найден в покупке purchase_id
    * */
    fun getProduct(
        purchase_id: Int,
        product_code: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var purchase = DBConnect.findPurchase(
            purchase_id,
            onSuccess = { purchase ->
                var obj = purchase.purchaseRowSet.find { it.product_code == product_code }
                onSuccess.invoke()
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )

    }
    /*fun getProduct(purchase_id: Int, product_code: Int): PurchaseRowDTO? {
        var purchase = DBConnect.findPurchase(purchase_id)
        if (purchase != null) {
            var obj = purchase.purchaseRowSet.find { it.product_code == product_code }
            if (obj != null) {
                return PurchaseRowToDTO(obj)
            }
        }
        return null
    }*/

    /*
    * Удаление продукта из чека
    * Кейс: клиент отказался от того или иного продукта до оплаты
    * Возвращает 0 в случае успеха (покупка найдена и открыта), иначе -1
    * */
    fun removeProductFromPurchase(
        purchase_id: Int,
        product_code: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var purchase = DBConnect.findPurchase(purchase_id,
            onSuccess = { purchase ->
                purchase.removeProduct(product_code)
                onRemoveSuccess(purchase, onSuccess, onError)
            },
            onError = { throwable ->
                onError.invoke(throwable)
            })

    }

    fun onRemoveSuccess(purchase: Purchase, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        DBConnect.updatePurchase(PurchaseToDTO(purchase),
            onSuccess = {
                onSuccess.invoke()
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )

    }

    /*
    * Добавление товара в чек.
    * Кейс: продукт отсканирован, его необходимо добавить.
    * Возвращает 0 в случае успеха (покупка найдена и открыта), иначе -1
    * */
    /*fun addProductToPurchase(purchase_id: Int, product_code: Int, count: Int): Int {
        var row = PurchaseRow(product_code, count)
        var purchase = DBConnect.findPurchase(purchase_id)
        if (purchase != null && purchase.status == Status.CREATED) {
            purchase.addProduct(row)
            DBConnect.updatePurchase(this.PurchaseToDTO(purchase))
            return 0
        } else {
            return -1
        }
    }*/
    fun addProductToPurchase(
        purchase_id: Int,
        product_code: Int,
        count: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var row = PurchaseRow(product_code, count)
        DBConnect.findPurchase(
            purchase_id,
            onSuccess = { purchase ->
                purchase.addProduct(row)
                DBConnect.updatePurchase(PurchaseToDTO(purchase),
                    onSuccess = {
                        onSuccess.invoke()
                    },
                    onError = { throwable ->
                        onError.invoke(throwable)
                    }
                )
            },
            onError = { throwable ->
                onError.invoke(throwable)
            }
        )
    }

    /*
    * Добавление дисконтной карты к покупке
    * Возвращает 0 в случае успеха (покупка найдена, открыта и в ней нет дисконтной карты), иначе -1
    * */
    fun addDiscountCard(
        purchase_id: Int,
        cardNum: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {

        DBConnect.findPurchase(purchase_id,
            onSuccess = { purchase ->
                if (purchase.status == Status.CREATED && purchase.discount_card == 0) {
                    purchase.discount_card = cardNum
                    DBConnect.updatePurchase(
                        PurchaseToDTO(purchase),
                        onSuccess = {
                            onSuccess.invoke()
                        },
                        onError = { throwable ->
                            onError.invoke(throwable)
                        }
                    )
                }
            },
            onError = { throwable ->
                onError.invoke(throwable)

            })
    }

    /*
    * Удаление дисконтной карты из покупки
    * Возвращает 0 в случае успеха (покупка найдена, открыта и в ней есть дисконтная карта),
    * иначе -1
    * */
//    fun removeDiscountCard(purchase_id: Int): Int {
//        var purchase = DBConnect.findPurchase(purchase_id)
//        if (purchase != null && purchase.status == Status.CREATED && purchase.discount_card != 0) {
//            purchase.discount_card = 0
//            DBConnect.updatePurchase(this.PurchaseToDTO(purchase))
//            return 0
//        } else {
//            return -1
//        }
//    }
    fun removeDiscountCard(
        purchase_id: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {

        DBConnect.findPurchase(purchase_id,
            onSuccess = { purchase ->
                if (purchase.status == Status.CREATED && purchase.discount_card != 0) {
                    purchase.discount_card = 0
                    DBConnect.updatePurchase(
                        PurchaseToDTO(purchase),
                        onSuccess = {
                            onSuccess.invoke()
                        },
                        onError = { throwable ->
                            onError.invoke(throwable)
                        }
                    )
                }
            },
            onError = { throwable ->
                onError.invoke(throwable)

            })
    }

    /*
    * Получение информации о покупке.
    * Кейс: вывод информации на кассу
    * Возвращает список покупок (product_code, count, цена за шт., скидка, итоговая цена)
    * */
    fun getPurchaseInfo(
        purchase_id: Int,
        onSuccess: () -> Unit,
        onError: (Throwable) -> Unit
    ) {
        var purchase = DBConnect.findPurchase(
            purchase_id,
            onSuccess = { purchase ->
                
            },
            onError = { throwable ->
                onError.invoke(throwable)
            })
    }
//    fun getPurchaseInfo(purchase_id: Int): PurchaseInfo {
//        var purchase = DBConnect.findPurchase(purchase_id)
//        var resultList = mutableListOf<PurchaseRowDTOview>()
//        var info: PurchaseInfo
//        if (purchase != null) {
//            var list = pricing.getDiscount(purchase.discount_card, purchase.purchaseRowSet)
//            var item: PurchaseRowDTOview
//            var price: Int
//            for (i in list) {
//                price = getProductPrice(i.product_code)
//                item = PurchaseRowDTOview(
//                    product_code = i.product_code,
//                    name = product.getProductById(i.product_code),
//                    count = i.count,
//                    type = i.type,
//                    discount = i.discount,
//                    price = price,
//                    result_price = (price - i.discount) * i.count
//                )
//                resultList.add(item)
//            }
//            var sum_price = resultList.sumBy { it.result_price }
//            var sum_discount = resultList.sumBy { it.discount }
//
//            return PurchaseInfo(
//                purchaseRows = resultList,
//                discount_card_id = purchase.discount_card,
//                sum_discount = sum_discount,
//                sum_price = sum_price,
//                price = (sum_price - sum_discount)
//            )
//        } else {
//            return PurchaseInfo(
//                purchaseRows = resultList,
//                discount_card_id = 0,
//                sum_discount = 0,
//                sum_price = 0,
//                price = 0
//            )
//
//        }
//    }

    /*
    * Получение стоимости продукта
    * */
    fun getProductPrice(product_code: Int): Int = pricing.getPrice(product_code)

    /*
    * Получение итоговой стоимости покупки
    * */
    fun getPurchasePrice(purchase_id: Int): Int {
        var pInfo = getPurchaseInfo(purchase_id)
        var sum: Int = 0
        for (i in pInfo.purchaseRows) {
            sum += i.result_price
        }
        return sum
    }

    /*
    * Получить отчет по покупке с учетом возвратов.
    * Т.е. берется покупка, ищутся по ней все возвраты, вычитаются возвращенные продукты
    * И формируется итоговый счёт, который возвращает метод
    * */
//    fun getBill(purchase_id: Int): PurchaseInfo {
//        var purchase = DBConnect.findPurchase(purchase_id)
//        var returns = DBConnect.findReturn(purchase_id)
//        if (purchase == null) {
//            return return PurchaseInfo(
//                purchaseRows = mutableListOf<PurchaseRowDTOview>(),
//                discount_card_id = 0,
//                sum_discount = 0,
//                sum_price = 0,
//                price = 0
//            )
//        }
//        var bill = getPurchaseInfo(purchase_id)
//        for (ret in returns) {
//            for (i in ret.purchaseRowSet) {
//                for (item in bill.purchaseRows) {
//                    if (i.product_code == item.product_code) {
//                        item.count -= i.count
//                        item.result_price = (item.price - item.discount) * item.count
//                    }
//                }
//            }
//        }
//        bill.purchaseRows.removeIf { it.count == 0 }
//        var sum_price = bill.purchaseRows.sumBy { it.result_price }
//        var sum_discount = bill.purchaseRows.sumBy { it.discount }
//        return PurchaseInfo(
//            purchaseRows = bill.purchaseRows,
//            discount_card_id = purchase.discount_card,
//            sum_discount = sum_discount,
//            sum_price = sum_price,
//            price = (sum_price - sum_discount)
//        )
//    }

    /*
    * Метод конвертации PurchaseRow в DataTransferObject
    * */
    private fun PurchaseRowToDTO(row: PurchaseRow): PurchaseRowDTO {
        return PurchaseRowDTO(row.product_code, row.count, row.type, row.discount)
    }

    /*
    * Метод конвертации Purchase в DataTransferObject
    * */
    private fun PurchaseToDTO(purchase: Purchase): PurchaseDTO {
        return PurchaseDTO(
            purchase.id,
            purchase.cashier,
            purchase.type,
            purchase.purchaseId,
            purchase.status,
            purchase.purchaseRowSet,
            purchase.create_dttm.toString(),
            purchase.discount_card
        )
    }

}