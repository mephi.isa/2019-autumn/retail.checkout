package com.example.checkout

interface Pricing {
    fun getPrice(barcode: Int): Int
    fun getDiscount(card_id:Int, bill: MutableSet<PurchaseRow>):MutableSet<PurchaseRow>
}