package com.example.checkout

import org.w3c.dom.Text

class PrinterMock : Printer {
    override fun print_bill(input: PurchaseInfo){
        for(i in input.purchaseRows){
            print(i)
            print("\n")
        }
    }
}