package com.example.checkout

enum class Status{
    CREATED,
    DECLINED,
    PAID
}