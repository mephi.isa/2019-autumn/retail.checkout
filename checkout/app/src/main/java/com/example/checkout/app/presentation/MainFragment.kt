package com.example.checkout.app.presentation

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.checkout.PurchaseRowDTOview
import com.example.checkout.R
import com.example.checkout.app.extension.observe
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Section
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(R.layout.fragment_main) {

    private lateinit var viewModel: MainViewModel

    private val topAnchorView = SimpleItem(R.layout.view_empty)
    private val productsSection = Section()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.context = requireContext()

        observe(viewModel.state, ::handleContent)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        with(checkout_recycler) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            adapter = com.xwray.groupie.GroupAdapter<GroupieViewHolder>().apply {
                add(topAnchorView)
                add(productsSection)
            }
        }
        btnScan.setOnClickListener {
            viewModel.scanAction()
        }
        btnNewPurchase.setOnClickListener {
            var pId = viewModel.newPurchaseAction()
            if (pId != -1) {
                tvCurPurIdVal.text = pId.toString()
            }
        }

        btnNewReturn.setOnClickListener {
            var dlg = AlertDialog.Builder(this.context)
            val view = layoutInflater.inflate(R.layout.alert_dialog, null)
            val edit = view.findViewById(R.id.etUserInput) as EditText
            dlg.setTitle("Возврат")
            dlg.setMessage("Введите номер чека")
            dlg.setView(view)
            dlg.setPositiveButton(android.R.string.yes) { dialog, which ->
                var retId = viewModel.newReturnAction(edit.text.toString().toInt())
                if (retId != -1) {
                    tvCurPurIdVal.text = retId.toString() + " (RET)"
                }
            }
            dlg.setNegativeButton(android.R.string.no) { dialog, which ->
                showMsg("Действие отменено!")
            }

            dlg.show()
        }

        btnPay.setOnClickListener {
            viewModel.payAction()
        }

        btnDecline.setOnClickListener {
            if (viewModel.declineAction() == 0) {
                tvCurPurIdVal.text = "-1"
            }
        }
    }

    private fun handleContent(viewState: ViewState) {
        val items = viewState.purchaseViews.map { purchase ->
            PurchaseItem(
                purchaseInfo = purchase,
                onIncClick = ::onPurchaseIncClick,
                onDecClick = ::onPurchaseDecClick,
                onRemClick = ::onPurchaseRemoveClick
            )
        }

        checkout_recycler.post {
            productsSection.update(items)
        }

        tvCurPurIdVal.text = viewState.currentPurchase
    }

    private fun onPurchaseIncClick(item: PurchaseRowDTOview) {
        viewModel.incrementProduct(item.product_code)
    }

    private fun onPurchaseDecClick(item: PurchaseRowDTOview) {
        viewModel.decrementProduct(item.product_code)

    }

    private fun onPurchaseRemoveClick(item: PurchaseRowDTOview) {
        viewModel.removeProduct(item.product_code)

    }

    fun showMsg(text: String) {
        Toast.makeText(this.context, text, Toast.LENGTH_SHORT).show()
    }

}
