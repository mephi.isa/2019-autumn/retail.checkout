package com.example.checkout.app.network

import com.google.gson.annotations.SerializedName

data class UpdResult(

    @SerializedName("success")
    val result: String,

    @SerializedName("ID")
    val id: Int

)