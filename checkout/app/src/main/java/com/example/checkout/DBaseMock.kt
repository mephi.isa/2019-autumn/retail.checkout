package com.example.checkout

import java.time.LocalDateTime

class DBaseMock : DBase {
    var purchases: MutableSet<Purchase> = mutableSetOf()

    override fun updatePurchase(pr: PurchaseDTO): Int {
        var p: Purchase = Purchase(
            id = pr.id,
            cashier = pr.cashier,
            type = pr.type,
            purchaseId = pr.purchaseId,
            status = pr.status,
            purchaseRowSet = pr.purchaseRowSet,
            create_dttm = LocalDateTime.parse(pr.create_dttm)
        )
        purchases.removeIf { it.id == p.id }
        p.discount_card = pr.discount_card
        purchases.add(p)
        return 1
    }

    override fun findPurchase(id: Int): Purchase? {
        return purchases.find { it.id == id }
    }

    override fun findReturn(purchase_id: Int): List<Purchase> {
        return purchases.filter { it.purchaseId == purchase_id && it.type == PurchaseType.RETURN }
    }
}