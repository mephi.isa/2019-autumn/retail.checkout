package com.example.checkout.app.presentation

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.checkout.*
import com.example.checkout.app.network.DataBaseRepository

class MainViewModel : ViewModel() {

    companion object {
        private const val NO_VALUE = "Значения нет"
    }

    lateinit var context: Context

    val state = MutableLiveData<ViewState>()

    var currentPurchaseId = -1
    var scanner = ScannerMock()
    var pricing = PricingMock()
    var product = ProductMock()
    var printer = PrinterMock()
    var cashier = 1
    var pinPad = PinPadMock()

    var viewState = ViewState(emptyList(), NO_VALUE)
        set(value) {
            state.value = value
            field = value
        }

    //var DBConnect = MongoDBase("10.0.2.2",27017, "lab_admin", "610", "checkout")
    //var DBConnect = MongoDBase("bjczqvfva3wh1g4-mongodb.services.clever-cloud.com",27017, "uszgqwmucmlbc0223x8y", "Cq4ixfVmuNBU8MSVRIe9", "bjczqvfva3wh1g4")
    //var DBConnect = DBaseMock()
    var checkout = Checkout(
        scanner = scanner,
        pricing = pricing,
        printer = printer,
        product = product,
        cashier = cashier,
        pinPad = pinPad,
        DBConnect = DataBaseRepository()
    )

    init {
        /*state.value = listOf(
            PurchaseRowDTOview(
                product_code = 11,
                name = "KDJD",
                count = 2,
                type = ProductType.PRODUCT,
                discount = 1,
                price = 10,
                result_price = 9
            ),
            PurchaseRowDTOview(
                product_code = 12,
                name = "KDdss22222JD",
                count = 2,
                type = ProductType.PRODUCT,
                discount = 1,
                price = 10,
                result_price = 9
            ),
            PurchaseRowDTOview(
                product_code = 13,
                name = "KDJlkdlkdldD",
                count = 2,
                type = ProductType.PRODUCT,
                discount = 1,
                price = 10,
                result_price = 9
            )

        )*/
    }

    fun scanAction() {
        var pCode = checkout.scanner.scan()
        var addResult = checkout.addProductToPurchase(currentPurchaseId, pCode, 1)
        if (addResult != -1) {
            viewState = viewState.copy(
                purchaseViews = checkout.getPurchaseInfo(currentPurchaseId).purchaseRows
            )
        }
    }

    fun newPurchaseAction(): Int {
        if (currentPurchaseId != -1) {
            showMsg("ERROR: Текущая покупка не закрыта!")
            return -1
        } else {
            var purch = checkout.newPurchase(-1)
            if (purch != null) {
                currentPurchaseId = purch.id
                var pInfo = checkout.getPurchaseInfo(currentPurchaseId)

                viewState = viewState.copy(
                    purchaseViews = pInfo.purchaseRows
                )

                showMsg("Ok")

                return currentPurchaseId
            } else {
                showMsg("ERROR: Purchase not created!")
                return -1
            }
        }
    }

    fun newReturnAction(purchaseId: Int): Int {
        if (currentPurchaseId != -1) {
            showMsg("ERROR: Текущая покупка не закрыта!")
            return -1
        } else {
            var ret = checkout.newReturn(-1, purchaseId)
            if (ret != null) {
                currentPurchaseId = ret.id
                return ret.id
            } else {
                showMsg("ERROR: Не удалось создать возвоат")
                return -1
            }
        }
    }

    fun payAction() {
        checkout.payPurchase(
            purchase_id = currentPurchaseId,
            onSuccess = {
                currentPurchaseId = -1
                viewState = viewState.copy(
                    currentPurchase = NO_VALUE
                )
            },
            onError = {
                showMsg("ERROR: Ошибка при оплате")
            }
        )
    }

    fun declineAction(): Int {
        var res = checkout.declinePurchase(currentPurchaseId)
        if (res == -1) {
            showMsg("ERROR: Ошибка при оплате")
            return -1
        } else {
            currentPurchaseId = -1
        }
        return 0
    }

    fun showMsg(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun incrementProduct(prod_id: Int) {
        if (checkout.addProductToPurchase(currentPurchaseId, prod_id, 1) == 0) {
            viewState = viewState.copy(
                purchaseViews = checkout.getPurchaseInfo(currentPurchaseId).purchaseRows
            )
        }
    }

    fun decrementProduct(prod_id: Int) {
        if (checkout.addProductToPurchase(currentPurchaseId, prod_id, -1) == 0) {
            viewState = viewState.copy(
                purchaseViews = checkout.getPurchaseInfo(currentPurchaseId).purchaseRows
            )
        }
    }

    fun removeProduct(prod_id: Int) {
        var res = checkout.removeProductFromPurchase(currentPurchaseId, prod_id)
        if (res == 0) {
            viewState = viewState.copy(
                purchaseViews = checkout.getPurchaseInfo(currentPurchaseId).purchaseRows
            )
            showMsg("Delete product " + prod_id.toString() + " from purchase " + currentPurchaseId.toString())
        } else {
            showMsg("ERROR: Ошибка при удалении продукта")
        }
    }

    fun addCard(card_num: Int) {
        checkout.addDiscountCard(currentPurchaseId, card_num)
    }

    fun removeCard() {
        checkout.removeDiscountCard(currentPurchaseId)
    }
}

data class ViewState(
    val purchaseViews: List<PurchaseRowDTOview>,
    val currentPurchase: String
)
