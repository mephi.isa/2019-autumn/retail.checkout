package com.example.checkout.app.network


import com.example.checkout.DBase
import com.example.checkout.Purchase
import com.example.checkout.PurchaseDTO
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataBaseRepository : DBase {

    override fun updatePurchase(
        purchaseDTO: PurchaseDTO,
        onSuccess: ((UpdResult) -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        NetworkService
            .getServerApi()
            .updatePurchase(purchaseDTO)
            .enqueue(object : Callback<UpdResult> {
                override fun onResponse(call: Call<UpdResult>, response: Response<UpdResult>) {
                    onSuccess.invoke(requireNotNull(response.body()))
                }

                override fun onFailure(call: Call<UpdResult>, throwable: Throwable) {
                    onError.invoke(throwable)
                }
            })
    }

    override fun findPurchase(
        purchaseId: Int,
        onSuccess: ((Purchase) -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        NetworkService
            .getServerApi()
            .findPurchase(purchaseId)
            .enqueue(object : Callback<Purchase> {
                override fun onResponse(call: Call<Purchase>, response: Response<Purchase>) {
                    onSuccess.invoke(requireNotNull(response.body()))
                }

                override fun onFailure(call: Call<Purchase>, throwable: Throwable) {
                    onError.invoke(throwable)
                }
            })
    }

    override fun findReturn(
        purchaseIdForReturn: Int,
        onSuccess: ((List<Purchase>) -> Unit),
        onError: ((Throwable) -> Unit)
    ) {
        NetworkService
            .getServerApi()
            .findReturn(purchaseIdForReturn)
            .enqueue(object : Callback<List<Purchase>> {
                override fun onResponse(
                    call: Call<List<Purchase>>,
                    response: Response<List<Purchase>>
                ) {
                    onSuccess.invoke(requireNotNull(response.body()))
                }

                override fun onFailure(call: Call<List<Purchase>>, throwable: Throwable) {
                    onError.invoke(throwable)
                }
            })
    }

}