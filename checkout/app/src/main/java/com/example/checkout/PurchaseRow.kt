package com.example.checkout

data class PurchaseRow(
    var product_code:Int,
    var count: Int = 1,
    var type:ProductType = ProductType.PRODUCT,
    var discount:Int = 0
){
}
