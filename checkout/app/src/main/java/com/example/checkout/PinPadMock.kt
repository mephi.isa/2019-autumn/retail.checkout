package com.example.checkout

class PinPadMock : PinPad {
    override fun card_pay(price:Int):Boolean = true
}