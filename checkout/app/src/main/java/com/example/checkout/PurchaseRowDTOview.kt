package com.example.checkout

import com.google.gson.annotations.SerializedName

/*
* Data Transfer Object для взаимодействия с объектами снаружи.
* Включает в себя стоимость и скидку.
* */
data class PurchaseRowDTOview(
    @SerializedName("product_code")
    var product_code:Int,

    @SerializedName("name")
    var name:String,

    @SerializedName("count")
    var count: Int,

    @SerializedName("type")
    var type:ProductType,

    @SerializedName("discount")
    var discount:Int,

    @SerializedName("price")
    var price:Int,

    @SerializedName("result_price")
    var result_price:Int
) {
}