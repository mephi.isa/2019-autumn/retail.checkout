package com.example.checkout

interface Product {
    fun getProductById(product_id: Int): String
}