package com.example.checkout

import com.example.checkout.app.network.UpdResult

interface DBase {

    fun updatePurchase(
        purchaseDTO: PurchaseDTO,
        onSuccess: ((UpdResult) -> Unit),
        onError: ((Throwable) -> Unit)
    )

    fun findPurchase(
        purchaseId: Int,
        onSuccess: ((Purchase) -> Unit),
        onError: ((Throwable) -> Unit)
    )

    fun findReturn(
        purchaseIdForReturn: Int,
        onSuccess: ((List<Purchase>) -> Unit),
        onError: ((Throwable) -> Unit)
    )
}