package com.example.checkout.app.presentation

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item

data class SimpleItem(private val layoutId: Int) : Item() {

    override fun getId() = layoutId.toLong()

    override fun getLayout(): Int = layoutId

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        // Do nothing
    }

}