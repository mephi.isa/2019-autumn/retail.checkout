package com.example.checkout.app.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object NetworkService {

    private const val BASE_URL = "10.0.2.2:8080"

    private val retrofit: Retrofit by lazy {

        val interceptor = HttpLoggingInterceptor().apply {
            level = Level.BODY
        }

        val client = OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    fun getServerApi(): ServerApi {
        return retrofit.create(ServerApi::class.java)
    }
}