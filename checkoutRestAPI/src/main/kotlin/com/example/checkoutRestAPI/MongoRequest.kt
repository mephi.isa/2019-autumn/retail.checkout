package com.example.checkoutRestAPI

import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.bson.BsonDocument
import org.bson.Document

class MongoRequest(
        host:String,
        port:Int,
        username:String,
        password:String,
        db_name:String
){
    private val credentials = MongoCredential.createCredential(username, db_name, password.toCharArray())
    private var options = MongoClientOptions.builder()
    .connectTimeout(0)
    .maxConnectionIdleTime(0)
    .maxWaitTime(0)
    .socketTimeout(3000000)
    .serverSelectionTimeout(500000)
    private var client = MongoClient(ServerAddress(host, port), credentials, options.build())
    private var dbase = client.getDatabase(db_name)
    private var collection = dbase.getCollection("purchases", BsonDocument::class.java)

    fun findPurchase(id:Int):String{
        var query = Document("id", id)
        var foundObjects = collection.find(query)
        var jsonObject = ""

        if(foundObjects.count() == 0){
            return "{\"error\": \"NotFound\"}"
        }

        for(i in foundObjects){
            jsonObject = i.toJson()
        }
        return jsonObject
    }

    fun findReturn(purchase_id: Int): String {
        var query = Document("purchaseId", purchase_id).append("type", "RETURN")
        var foundObjects = collection.find(query)
        var purchases: MutableList<String> = mutableListOf()
        var jsonObject = mutableListOf<String>()
        if(foundObjects.count() == 0){
            return "{\"error\": \"NotFound\"}"
        }

        for(i in foundObjects){
            jsonObject.add(i.toJson())
        }
        return jsonObject.toString()
    }

    fun update(id:Int, purchaseJson: String):String{
        var query = BsonDocument.parse(purchaseJson)
        var filter = Document("id", id)
        var found = collection.find(filter).count()
        if(found > 0){
            collection.replaceOne(filter, query)
            return "{\"success\": \"Updated\"}"
        }
        else{
            collection.insertOne(query)
            return "{\"success\": \"Inserted\"}"
        }
    }
}