package com.example.checkoutRestAPI

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CheckoutRestApiApplication

fun main(args: Array<String>) {
	runApplication<CheckoutRestApiApplication>(*args)
}
