package main


data class Check(
    var id:Int,
    var cashier:Int,
    var type:CheckType = CheckType.CHECK,
    var status:Status = Status.CREATED,
    var purchaseRowSet: MutableSet<CheckRow> = mutableSetOf<CheckRow>()
) {
    var discountCard:Int = 0
        get() = field
        set(value){
            if(value >= 0){
                field = value
            }
        }



    fun decline(){
        this.status = Status.DECLINED
    }

    fun close(){
        this.status = Status.PAID
    }

    fun removeProduct(product_code:Int){
        if(this.status == Status.CREATED) {
            purchaseRowSet.removeIf { it.product_code == product_code }
        }
    }

    fun findProduct(product_code:Int): CheckRow? = purchaseRowSet.find { it.product_code == product_code }

    fun addProduct(row:CheckRow){
        if(this.status == Status.CREATED) {
            var prod = findProduct(product_code = row.product_code)
            if(prod == null){
                purchaseRowSet.add(row)
            }
            else{
                prod.count += row.count
            }
        }
    }

    fun applyCard(discountCard: Int){

    }

    fun checkCard(){

    }

}