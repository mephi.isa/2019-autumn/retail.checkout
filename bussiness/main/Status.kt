package main

enum class Status {
    CREATED,
    DECLINED,
    PAID
}