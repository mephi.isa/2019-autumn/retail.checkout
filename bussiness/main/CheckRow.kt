package main

data class CheckRow(
    var product_code:Int,
    var count: Int = 1
){

    fun addCount(count: Int){
        this.count += count;
    }

    fun removeCount(count: Int){
        if (count >= 0)
            this.count = count;
        else
            throw Exception("Product count in check cant be less than 0");
    }
}
