package models

import com.mark.main.model.Check
import com.mark.main.model.CheckRow
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CheckRowTest {
    private var count = 0;
    private var checkRow: CheckRow? = null;

    @Before
    fun init() {
        checkRow = CheckRow(1, 1);
    }

    @Test
    fun getCode_ReturnCode_CodeReturned() {
        val expectedCode = 1;
        Assert.assertEquals(expectedCode, checkRow?.productCode);
    }

    @Test
    fun getCount_ReturnCount_CountReturned() {
        val expectedCount = 1;
        Assert.assertEquals(expectedCount, checkRow?.count);
    }

    @Test
    fun addCount_AddToCheck_CountAdded() {
        checkRow?.addCount(2);
        Assert.assertEquals(3, checkRow?.count);
    }

    @Test
    fun removeCount_RemoveFromCheck_CountRemoved() {
        checkRow?.removeCount(1);
        Assert.assertEquals(0, checkRow?.count);
    }

    @Test(expected = Exception::class)
    fun removeCount_CantRemove_ExceptionThrown() {
        checkRow?.removeCount(2);
    }


}