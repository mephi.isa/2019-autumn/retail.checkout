package models

import com.mark.main.model.Check
import com.mark.main.model.CheckRow
import com.mark.main.model.CheckType
import com.mark.main.model.Status
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.lang.NullPointerException
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CheckTest{
    private var check: Check? = null;
    private var cashierId: Int = 1;
    private var checkId: Int = 101;
    private var purchaseRowSet: MutableSet<CheckRow> = mutableSetOf<CheckRow>()
    private var status: Status = Status.CREATED;
    private var checkType: CheckType = CheckType.CHECK;

    @Before
    fun init() {
        check = Check(checkId, cashierId, checkType, status, purchaseRowSet);
    }

    @Test
    fun getCashier_ReturnCheckCashier_CashierReturned() {
        val expectedCashier = 1;
        Assert.assertEquals(expectedCashier, check?.cashier);
    }

    @Test
    fun getCheck_ReturnCheck_CheckReturned() {
        val expectedClass = Check::class.java;
        Assert.assertEquals(expectedClass,  check!!::class.java);
    }

    @Test
    fun getCard_ReturnCheckCard_CardReturned() {
        val expectedCard = 101;
        check?.discountCard = 101;
        Assert.assertEquals(expectedCard, check?.discountCard);
    }

    @Test
    fun addProduct_NewProduct_ProductAdded() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
        Assert.assertTrue(check?.purchaseRowSet?.contains(row)!!);
    }

    @Test
    fun removeProduct_DeleteProduct_ProductDeleted() {
        val row = CheckRow(1, 2);
        check?.addProduct(row);
        check?.removeProduct(1, 1);
        Assert.assertEquals(1, row.count);
    }

    @Test
    fun removeProduct_CountMoreThanCheck_FullyRemoved() {
        val row = CheckRow(1, 2);
        check?.addProduct(row);
        check?.removeProduct(1, 3);
        Assert.assertFalse(check?.purchaseRowSet?.contains(row)!!);
    }

    @Test
    fun addProduct_AddSameProd_CountAdded() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
        check?.addProduct(row);
        val expectedCount = 2;
        Assert.assertEquals(expectedCount, row.count);
    }

    @Test
    fun newCheck_CreatedCheckStatus_CreatedReturned() {
        val expectedStatus = Status.CREATED;
        Assert.assertEquals(expectedStatus, check?.status);
    }

    @Test
    fun paidCheck_PaidCheckStatus_PaidReturned() {
        val expectedStatus = Status.PAID;
        check?.close();
        val actualStatus = check?.status;
        Assert.assertEquals(expectedStatus, actualStatus);
    }

    @Test(expected = Exception::class)
    fun paidCheck_UnableWorkPaid_ExceptionThrown() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
//        check?.findProduct(1);
        check?.close();
        val rowPaid = CheckRow(2, 2);
        check?.addProduct(rowPaid);
//        check?.removeProduct(1);

    }

    @Test(expected = Exception::class)
    fun declinedCheck_UnableWorkDeclined_ExceptionThrown() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
        check?.findProduct(1);
        check?.decline();
        val rowDeclined = CheckRow(3, 3);
        check?.addProduct(rowDeclined);
//        check?.removeProduct(3);
    }

    @Test
    fun declinedCheck_DeclinedCheckStatus_DeclinedReturned() {
        val expectedStatus = Status.DECLINED;
        check?.decline();
        val actualStatus = check?.status;
        Assert.assertEquals(expectedStatus, actualStatus);
    }
    //Добавить тесты: новая покупка в created, оплаченная покупка в paid, отклоненная покупка в declined
    //невозможность работать с покупкой, если она в другом состоянии


    @Test
    fun findProduct_SearchWithProductCode_ProductFound() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
        val rowR = check?.findProduct(1);
        Assert.assertEquals(rowR,row);
    }

    @Test
    fun findProduct_SearchWithProductCode_ProductNotFound() {
        val row = CheckRow(1, 1);
        check?.addProduct(row);
        val rowR = check?.findProduct(2);
        Assert.assertNotEquals(rowR,row);
    }

    @Test
    fun applyCard_CardToCheck_CardAdded() {
        val expectedCard = 101;
        check?.applyCard(101);
        val actualCard = check?.checkCard();
        Assert.assertEquals(expectedCard, actualCard);
    }

    @Test(expected = Exception::class)
    fun applyCard_ValueLessZero_ExceptionThrown() {
        check?.applyCard(-101);
    }

    @Test
    fun checkCard_CardExists_CardReturned() {
        check?.applyCard(101);
        Assert.assertEquals(101, check?.checkCard());
    }

    @Test(expected = Exception::class)
    fun checkCard_CardNotExists_ExceptionThrown() {
        check?.checkCard();

//        val expectedValue = -1;
//        Assert.assertEquals(expectedValue, check?.checkCard());
    }

}